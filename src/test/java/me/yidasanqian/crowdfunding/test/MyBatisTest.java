package me.yidasanqian.crowdfunding.test;

import me.yidasanqian.crowdfunding.domain.User;
import me.yidasanqian.crowdfunding.domain.UserAuthc;
import me.yidasanqian.crowdfunding.service.impl.UserAuthcService;
import me.yidasanqian.crowdfunding.service.impl.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.Resource;

/**
 * @author Linyu Chen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MyBatisTest {

    @Resource
    private DataSourceTransactionManager transactionManager;

    @Resource
    private UserAuthcService userAuthcService;

    @Resource
    private UserService userService;

    @Transactional(rollbackFor = Exception.class)
    @Test
    public void testSaveRelation() {
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName("registerUser tx");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        System.out.println("MyBatisTest.testSaveRelation status : " + status.toString());
        try {
            UserAuthc userAuthc = new UserAuthc();
            userAuthc.setCredential("1111");
            userAuthc.setIdentifier("18181818181");
            userAuthc.setIdentityType(1);
            userAuthcService.save(userAuthc);
            User user = new User();
            int a = 1 / 0;
            user.setUserAuthcId(userAuthc.getId());
            userService.save(user);
        } catch (Exception e) {
            TransactionStatus status2 = TransactionAspectSupport.currentTransactionStatus();
            System.out.println("MyBatisTest.testSaveRelation status2 : " + status2.toString());
            status2.setRollbackOnly();
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testSave() {
        UserAuthc userAuthc = new UserAuthc();
        userAuthc.setCredential("1111");
        userAuthc.setIdentifier("18181818181");
        userAuthc.setIdentityType(1);
        userAuthcService.save(userAuthc);
    }
}
