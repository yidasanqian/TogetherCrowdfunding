package me.yidasanqian.crowdfunding.test;

import org.junit.Test;

/**
 * @author Linyu Chen
 */
public class TmpDirTest {

    @Test
    public void testPrintTmpDir() {

        String tmpDir = System.getProperty("java.io.tmpdir");
        System.out.println("TmpDirTest.testPrintTmpDir tmpDir<==" + tmpDir);
    }
}
