package me.yidasanqian.crowdfunding.test;

import org.junit.Test;
import org.springframework.util.StringUtils;

/**
 * @author Linyu Chen
 */
public class StringTest {

    @Test
    public void testStringArr() {
        int userId = 1;
        String follow = "23|54|1|3|2|";
        String result = handlerUserIdsString(userId, follow);
        System.out.println("StringTest.testStringArr ==> " + result);
    }

    private String handlerUserIdsString(int userId, String dbString) {
        String[] followArr = dbString.split("\\|");
        for (int i = 0; i < followArr.length; i++) {
            String f = followArr[i];
            if (!StringUtils.isEmpty(f) && f.equals(String.valueOf(userId))) {
                followArr[i] = "";
                break;
            }
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < followArr.length; i++) {
            String f = followArr[i];
            if (!StringUtils.isEmpty(f)) {
                sb.append(f);
                sb.append("|");
            }
        }
        return sb.toString();
    }

    @Test
    public void subStringTest() {
        String order = "alipay_sdk=alipay-sdk-java-dynamicVersionNo&app_id=2017072507886220&biz_content=%7B%0A%22body%22%3A%22%E5%AF%B9%E4%B8%80%E7%AC%94%E4%BA%A4%E6%98%93%E7%9A%84%E5%85%B7%E4%BD%93%E6%8F%8F%E8%BF%B0%E4%BF%A1%E6%81%AF%E3%80%82%E5%A6%82%E6%9E%9C%E6%98%AF%E5%A4%9A%E7%A7%8D%E5%95%86%E5%93%81%EF%BC%8C%E8%AF%B7%E5%B0%86%E5%95%86%E5%93%81%E6%8F%8F%E8%BF%B0%E5%AD%97%E7%AC%A6%E4%B8%B2%E7%B4%AF%E5%8A%A0%E4%BC%A0%E7%BB%99body%E3%80%82%22%2C%0A%22subject%22%3A%22%E5%A4%A7%E4%B9%90%E9%80%8F%22%2C%0A%22out_trade_no%22%3A%2270501111111S001111119%22%2C%0A%22timeout_express%22%3A%2290m%22%2C%0A%22total_amount%22%3A%220.01%22%2C%0A%22seller_id%22%3A%222088421666152599%22%2C%0A%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%2C%0A%22tips%22%3A%22%E6%B5%8B%E8%AF%95%E4%B8%80%E7%AC%94%E6%94%AF%E4%BB%98%22%0A%7D%0A&charset=UTF-8&format=json&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fbeihai.ngrok.cc%2Fapi%2Fv1%2Falipay%2FcheckAsyncNotify&sign=QPmk5%2F5E2nqs9wRWjSPJuM4e9M6AP0nkmDLH9XGcjW04eZTzH82SROV2wzdzQsLTFxLumgoK9n66oM7eM6MkiVWaypwNIcFu6pIsO17qzap3gDGYsnkuxqKozRISab7lDckYr7wzgD6Gja5mWwcpbDbK0cYdX%2FkODftfY3z8OrHQ1H0DCOXB%2Foz%2B7c4Ul8Osg9VpAI2QztpArkxW%2F7aPrLF56R9xgXuNmzHTvtGlJc38KzxgTIIxtrQprwAtn5tTyLRHSTSm6wynDgOxUf%2BL6%2BUhp4ABtuaKBbTRA6IBclSzos%2B7jzg7QxSGDcnPE%2FtHiH8Dd%2By3P2K%2B1WE3mFGKlg%3D%3D&sign_type=RSA2&timestamp=2017-07-28+17%3A56%3A12&version=1.0";
        int beginIndex = order.indexOf("&") + 1;
        String tmpOrder = order.substring(beginIndex, order.length());
        System.out.println("StringTest.subStringTest tmpOrder ==> " + tmpOrder);
    }
}
