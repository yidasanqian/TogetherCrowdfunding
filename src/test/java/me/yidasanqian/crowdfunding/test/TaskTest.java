package me.yidasanqian.crowdfunding.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author Linyu Chen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TaskTest {

    @Resource
    private Task task;


    @Test
    public void task() throws Exception {
        task.doTaskOne();
        task.doTaskTwo();
        task.doTaskThree();
    }
}
