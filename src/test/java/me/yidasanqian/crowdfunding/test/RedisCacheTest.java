package me.yidasanqian.crowdfunding.test;

import me.yidasanqian.crowdfunding.app.AddressApi;
import me.yidasanqian.crowdfunding.domain.Address;
import me.yidasanqian.crowdfunding.service.impl.AddressService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;

/**
 * @author Linyu Chen
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AddressApi.class)
public class RedisCacheTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AddressService addressService;

    @Test
    public void testCache() throws Exception {
        Address address = new Address();
        given(addressService.getAddress(1L))
                .willReturn(address);
        mvc.perform(MockMvcRequestBuilders
                .get("api/v1/address/getAddress")
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
