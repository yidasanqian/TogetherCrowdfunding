package me.yidasanqian.crowdfunding.test;

import me.yidasanqian.crowdfunding.domain.UserAuthc;
import me.yidasanqian.crowdfunding.util.PasswordHelper;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.Test;

/**
 * @author Linyu Chen
 */
public class UserTest {

    @Test
    public void md5Password() {
        String username = "admin123";
        String password = "666666";
        String credentialsSalt = username + "yidasanqian";

        //组合username,两次迭代，对密码进行加密
        String password_cipherText = new Md5Hash(password, credentialsSalt, 2).toHex();
        System.out.println("UserTest.md5Password salt <==" + credentialsSalt + "\tpass <==" + password_cipherText);

        UserAuthc userAuthc = new UserAuthc();
        userAuthc.setIdentifier(username);
        userAuthc.setCredential(password);
        PasswordHelper.encryptPassword(userAuthc);
        System.out.println("UserTest.md5Password userAuthc <== " + userAuthc.toString());
    }
}
