package me.yidasanqian.crowdfunding.component;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Enumeration;

/**
 * @author Linyu Chen
 */
@Aspect
@Component
public class WebLogAspect {
    public final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 定义一个切入点.
     * <p>
     * 第一个 * 代表任意修饰符及任意返回值.
     * 第二个 * 任意包名
     * 第三个 * 代表任意方法.
     * .. 匹配任意数量的参数.
     */
    @Pointcut("execution(public * me.yidasanqian.crowdfunding.app.*.*(..))")
    public void appMethodUseTime() {
    }

    @Pointcut("execution(public * me.yidasanqian.crowdfunding.web.*.*(..))")
    public void webMethodUseTime() {
    }

    /**
     * 记录每个公共方法的运行时间
     *
     * @param joinPoint
     * @return
     */
    @Around("appMethodUseTime()")
    public Object doAroundAppMethodUseTime(ProceedingJoinPoint joinPoint) {
        long start = System.currentTimeMillis();
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        // 记录下请求内容
        log.info("====== 开始记录请求内容 ======");
        log.info("URL : " + request.getRequestURL().toString());
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        // 获取所有参数方法一：
        Enumeration<String> enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            log.info(paraName + ": " + request.getParameter(paraName));
        }
        log.info("====== 结束记录请求内容 ======");

        Object result = null;
        try {

            result = joinPoint.proceed();
            long end = System.currentTimeMillis();
            log.info("doAroundAppMethodUseTime ==> " + joinPoint + "\t用时 : " + (end - start) + " ms!");
        } catch (Throwable e) {
            long end = System.currentTimeMillis();
            log.error("doAroundAppMethodUseTime ==> " + joinPoint + "\t用时 : " + (end - start) + " ms \n异常信息 : ", e);
        }
        return result;
    }

    @Around("webMethodUseTime()")
    public Object doAroundWebMethodUseTime(ProceedingJoinPoint joinPoint) {
        long start = System.currentTimeMillis();
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        // 记录下请求内容
        log.info("====== 开始记录请求内容 ======");
        log.info("URL : " + request.getRequestURL().toString());
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        // 获取所有参数方法一：
        Enumeration<String> enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            log.info(paraName + ": " + request.getParameter(paraName));
        }
        log.info("====== 结束记录请求内容 ======");

        Object result = null;
        try {
            result = joinPoint.proceed();
            long end = System.currentTimeMillis();
            log.info("doAroundWebMethodUseTime ==> " + joinPoint + "\t用时 : " + (end - start) + " ms!");
        } catch (Throwable e) {
            long end = System.currentTimeMillis();
            log.error("doAroundWebMethodUseTime ==> " + joinPoint + "\t用时 : " + (end - start) + " ms \n异常信息 : ", e);
        }
        return result;
    }
}
