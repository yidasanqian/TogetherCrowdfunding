package me.yidasanqian.crowdfunding.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * @author Linyu Chen
 */
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 角色标识程序中判断使用,如"admin",这个是唯一的
     */
    private String role;

    /**
     * 角色描述,UI界面显示使用
     */
    private String description;

    /**
     * 是否可用,如果不可用将不会添加给用户
     */
    private Boolean available;

    private Timestamp gmtCreate;

    private Timestamp gmtModified;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取角色标识程序中判断使用,如"admin",这个是唯一的
     *
     * @return role - 角色标识程序中判断使用,如"admin",这个是唯一的
     */
    public String getRole() {
        return role;
    }

    /**
     * 设置角色标识程序中判断使用,如"admin",这个是唯一的
     *
     * @param role 角色标识程序中判断使用,如"admin",这个是唯一的
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * 获取角色描述,UI界面显示使用
     *
     * @return description - 角色描述,UI界面显示使用
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置角色描述,UI界面显示使用
     *
     * @param description 角色描述,UI界面显示使用
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取是否可用,如果不可用将不会添加给用户
     *
     * @return available - 是否可用,如果不可用将不会添加给用户
     */
    public Boolean getAvailable() {
        return available;
    }

    /**
     * 设置是否可用,如果不可用将不会添加给用户
     *
     * @param available 是否可用,如果不可用将不会添加给用户
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }
}