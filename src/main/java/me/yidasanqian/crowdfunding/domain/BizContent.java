package me.yidasanqian.crowdfunding.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Linyu Chen
 */
public class BizContent implements Serializable {

    /**
     * body : 冲天高塔指苍穹， 燃烧生命照亮明天。好战友好兄弟， 你的故事我藏心底，准备好向前冲 ，我的背后就交给你，你的名字我不忘记——魁拔军歌
     * subject : 远天的战歌，伟大的妖侠——奇幻动画电影《魁拔IV》回归！
     * out_trade_no : 70501111111S001111119
     * timeout_express : 90m
     * total_amount : 0.01
     * seller_id : 2088421666152599
     * product_code : QUICK_WAP_WAY
     * rewardId : 2
     * userId : 12
     * number : 1
     * fundId : 1
     */

    private String body;
    private String subject;
    @SerializedName("out_trade_no")
    private String outTradeNo;
    @SerializedName("timeout_express")
    private String timeoutExpress;
    @SerializedName("total_amount")
    private BigDecimal totalAmount;
    @SerializedName("seller_id")
    private String sellerId;
    @SerializedName("product_code")
    private String productCode;
    private Integer number;
    private Long rewardId;
    private Long userId;
    private Long fundId;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTimeoutExpress() {
        return timeoutExpress;
    }

    public void setTimeoutExpress(String timeoutExpress) {
        this.timeoutExpress = timeoutExpress;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long rewardId) {
        this.rewardId = rewardId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    @Override
    public String toString() {
        return "BizContent{" +
                "body='" + body + '\'' +
                ", subject='" + subject + '\'' +
                ", outTradeNo='" + outTradeNo + '\'' +
                ", timeoutExpress='" + timeoutExpress + '\'' +
                ", totalAmount=" + totalAmount +
                ", sellerId='" + sellerId + '\'' +
                ", productCode='" + productCode + '\'' +
                ", rewardId=" + rewardId +
                ", userId=" + userId +
                ", number=" + number +
                ", fundId=" + fundId +
                '}';
    }
}
