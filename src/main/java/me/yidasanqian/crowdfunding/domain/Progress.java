package me.yidasanqian.crowdfunding.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Linyu Chen
 */
public class Progress implements Serializable {

    private static final long serialVersionUID = -8205857741929628594L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 目标金额
     */
    private BigDecimal goalFund;
    /**
     * 已筹金额
     */
    private BigDecimal hasFund;
    /**
     * 需要多少天
     */
    private Integer needDay;
    /**
     * 剩余天数
     */
    private Integer surplusDay;
    /**
     * 支持用户，存储格式"用户1 id|用户2 id|用户3 id"
     */
    private String surportPeople;

    private Timestamp gmtCreate;

    private Timestamp gmtModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getGoalFund() {
        return goalFund;
    }

    public void setGoalFund(BigDecimal goalFund) {
        this.goalFund = goalFund;
    }

    public BigDecimal getHasFund() {
        return hasFund;
    }

    public void setHasFund(BigDecimal hasFund) {
        this.hasFund = hasFund;
    }

    public Integer getNeedDay() {
        return needDay;
    }

    public void setNeedDay(Integer needDay) {
        this.needDay = needDay;
    }

    public Integer getSurplusDay() {
        return surplusDay;
    }

    public void setSurplusDay(Integer surplusDay) {
        this.surplusDay = surplusDay;
    }

    public String getSurportPeople() {
        return surportPeople;
    }

    public void setSurportPeople(String surportPeople) {
        this.surportPeople = surportPeople;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }
}