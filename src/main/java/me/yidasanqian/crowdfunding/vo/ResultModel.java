package me.yidasanqian.crowdfunding.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * <code>
 * {
 *      code：0,
 *      message: "success",
 *      data: { key1: value1, key2: value2, ... }
 * }
 * </code>
 * <p>
 *  code: 返回码，0表示成功，非0表示各种不同的错误。<br>
 *  message: 描述信息，成功时为"success"，错误时则是错误信息。<br>
 *  data: 成功时返回的数据，类型为对象或数组。
 *
 * @author Linyu Chen
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultModel {

    private int code;
    private String message;
    private Object data;

    private ResultModel(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /** 请求成功，无返回信息*/
    public static ResultModel success() {

        return new ResultModel(0, "success", null);
    }

    /** 请求成功，有返回信息*/
    public static ResultModel success(Object data) {
        return new ResultModel(0, "success", data);
    }

    /** 约定的请求错误，返回约定的提示信息*/
    public static ResultModel fail(int code, String failMsg) {
        return new ResultModel(code, failMsg, null);
    }

    /** 异常情况*/
    public static ResultModel error(int code, String errorMsg) {
        return new ResultModel(code, errorMsg,null);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
