package me.yidasanqian.crowdfunding.vo;

import me.yidasanqian.crowdfunding.domain.Media;
import me.yidasanqian.crowdfunding.domain.User;

import java.io.Serializable;

/**
 * @author Linyu Chen
 */
public class UserVo implements Serializable {
    private static final long serialVersionUID = 4929768018492807288L;
    private User user;
    private Media media;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }
}
