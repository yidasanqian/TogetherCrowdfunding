package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.CrowdFund;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CrowdFundMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CrowdFund record);

    CrowdFund selectByPrimaryKey(Long id);

    List<CrowdFund> selectAll();

    int updateByPrimaryKey(CrowdFund record);

    List<CrowdFund> listByUserId(Long userId);

    List<CrowdFund> listCrowdFund(Long fundId);

    CrowdFund getByUserIdAndFundId(@Param("userId") Long userId,
                                   @Param("fundId") Long fundId);
}