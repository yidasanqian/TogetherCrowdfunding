package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Reward;

import java.util.List;

public interface RewardMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Reward record);

    Reward selectByPrimaryKey(Long id);

    List<Reward> selectAll();

    int updateByPrimaryKey(Reward record);

    List<Reward> listByFundId(Long fundId);
}