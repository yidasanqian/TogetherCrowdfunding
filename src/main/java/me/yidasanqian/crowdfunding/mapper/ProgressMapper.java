package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Progress;

import java.util.List;

public interface ProgressMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Progress record);

    Progress selectByPrimaryKey(Long id);

    List<Progress> selectAll();

    int updateByPrimaryKey(Progress record);
}