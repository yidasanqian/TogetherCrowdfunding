package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.RelationFundTag;

import java.util.List;

public interface RelationFundTagMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RelationFundTag record);

    RelationFundTag selectByPrimaryKey(Long id);

    List<RelationFundTag> selectAll();

    int updateByPrimaryKey(RelationFundTag record);
}