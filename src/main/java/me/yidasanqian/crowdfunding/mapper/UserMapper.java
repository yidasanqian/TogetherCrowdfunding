package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.User;

import java.util.List;

public interface UserMapper {
    Long deleteByPrimaryKey(Long id);

    Long insert(User record);

    User selectByPrimaryKey(Long id);

    List<User> selectAll();

    Long updateByPrimaryKey(User record);

    Long getUserId(Long userAuthsId);

    User getUserByAuthcId(Long authcId);
}