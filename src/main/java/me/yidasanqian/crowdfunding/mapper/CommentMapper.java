package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Comment;
import me.yidasanqian.crowdfunding.vo.CommentVo;

import java.util.List;

public interface CommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Comment record);

    Comment selectByPrimaryKey(Long id);

    List<Comment> selectAll();

    int updateByPrimaryKey(Comment record);

    List<CommentVo> listCommentsByFundId(Long fundId);
}