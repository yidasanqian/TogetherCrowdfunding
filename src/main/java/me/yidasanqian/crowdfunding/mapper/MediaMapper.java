package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Media;

import java.util.List;

public interface MediaMapper {
    int deleteByPrimaryKey(Long id);

    Long insert(Media record);

    Media selectByPrimaryKey(Long id);

    List<Media> selectAll();

    int updateByPrimaryKey(Media record);
}