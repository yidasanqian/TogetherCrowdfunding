package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.UserAuthc;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserAuthcMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserAuthc record);

    UserAuthc selectByPrimaryKey(Long id);

    List<UserAuthc> selectAll();

    int updateByPrimaryKey(UserAuthc record);

    String getIdentifier(String identifier);

    UserAuthc getUserAuthc(@Param("identifier") String username,
                           @Param("credential") String password);

    UserAuthc getUserAuthcByIdentifier(String username);
}