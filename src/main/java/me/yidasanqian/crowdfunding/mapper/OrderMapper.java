package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Order record);

    Order selectByPrimaryKey(Long id);

    List<Order> selectAll();

    int updateByPrimaryKey(Order record);

    List<Order> listOrder(@Param("userId") Long userId, @Param("status") Integer status);

    Order getByOutTradeNo(String outTradeNo);
}