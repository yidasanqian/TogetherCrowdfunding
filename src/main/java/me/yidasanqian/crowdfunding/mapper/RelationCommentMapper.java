package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.RelationComment;

import java.util.List;

public interface RelationCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RelationComment record);

    RelationComment selectByPrimaryKey(Long id);

    List<RelationComment> selectAll();

    int updateByPrimaryKey(RelationComment record);
}