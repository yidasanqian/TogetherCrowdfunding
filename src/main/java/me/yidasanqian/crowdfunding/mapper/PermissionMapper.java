package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Permission;
import me.yidasanqian.crowdfunding.util.MyMapper;

public interface PermissionMapper extends MyMapper<Permission> {
}