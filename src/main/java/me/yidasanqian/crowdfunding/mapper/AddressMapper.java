package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Address;

import java.util.List;

public interface AddressMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Address record);

    Address selectByPrimaryKey(Long id);

    List<Address> selectAll();

    int updateByPrimaryKey(Address record);

    int count(Long userId);

    List<Address> listAddress(Long userId);
}