package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.PayOrder;

import java.util.List;

public interface PayOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PayOrder record);

    PayOrder selectByPrimaryKey(Long id);

    List<PayOrder> selectAll();

    int updateByPrimaryKey(PayOrder record);
}