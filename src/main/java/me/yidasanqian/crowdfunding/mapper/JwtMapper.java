package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Jwt;

import java.util.List;

public interface JwtMapper {
    int insert(Jwt record);

    List<Jwt> selectAll();

    void deleteByUserId(Long userId);

    boolean isExist(String token);

    Jwt getByUserId(Long userId);

    void update(Jwt jwt);
}