package me.yidasanqian.crowdfunding.mapper;

import me.yidasanqian.crowdfunding.domain.Permission;
import me.yidasanqian.crowdfunding.domain.Role;
import me.yidasanqian.crowdfunding.util.MyMapper;

import java.util.List;

public interface RoleMapper extends MyMapper<Role> {
    List<Role> listRolesByUserId(Long userId);

    List<Permission> listPermisionsByRoleId(Long roleId);

    List<Role> listRolesByUsername(String username);
}