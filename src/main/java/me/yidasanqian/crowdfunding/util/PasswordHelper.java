package me.yidasanqian.crowdfunding.util;

import me.yidasanqian.crowdfunding.domain.UserAuthc;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;

/**
 * 使用Shiro的md5哈希加密密码
 * @author Linyu Chen
 */
public class PasswordHelper {

    /**
     * 公盐
     */
    private static final String salt = "yidasanqian";

    private static final int hashIterations = 2;

    public static void encryptPassword(UserAuthc userAuthc) {
        String credentialsSalt = userAuthc.getIdentifier() + salt;
        String newPassword = new Md5Hash(
                userAuthc.getCredential(),
                ByteSource.Util.bytes(credentialsSalt), hashIterations
        ).toHex();
        userAuthc.setSalt(salt);
        userAuthc.setCredential(newPassword);
    }
}
