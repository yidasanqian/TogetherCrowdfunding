package me.yidasanqian.crowdfunding.app;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.yidasanqian.crowdfunding.domain.Order;
import me.yidasanqian.crowdfunding.service.IOrderService;
import me.yidasanqian.crowdfunding.vo.ResultModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Linyu Chen
 */
@RequestMapping("api/v1/order")
@RestController
public class OrderApi {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IOrderService orderService;

    @RequestMapping(value = "listOrder", method = RequestMethod.GET)
    public Object listOrder(@RequestParam("userId") Long userId,
                            @RequestParam("status") Integer status,
                            @RequestParam("pn") Integer pn,
                            @RequestParam("ps") Integer ps) {

        Page<Order> orderPage = PageHelper.startPage(pn, ps);
        List<Order> orderList = orderService.listOrder(userId, status);
        int total = (int) orderPage.getTotal();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("orderList", orderList);
        data.put("total", total);
        return ResultModel.success(data);
    }
}
