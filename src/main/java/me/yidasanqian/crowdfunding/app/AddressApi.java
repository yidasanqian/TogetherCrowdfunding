package me.yidasanqian.crowdfunding.app;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.yidasanqian.crowdfunding.domain.Address;
import me.yidasanqian.crowdfunding.service.IAddressService;
import me.yidasanqian.crowdfunding.vo.ResultModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Linyu Chen
 */
@RestController
@RequestMapping("api/v1/address")
public class AddressApi {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private IAddressService addressService;

    @RequestMapping(value = "addAddress", method = RequestMethod.POST)
    public Object addAddress(Address address) {
        int count = addressService.countAddress(address.getUserId());
        if (count < 1) {
            address.setIsDefault(1);
        } else {
            address.setIsDefault(0);
        }
        addressService.save(address);
        return ResultModel.success();
    }

    @RequestMapping(value = "deleteAddress", method = RequestMethod.POST)
    public Object deleteAddress(@RequestParam("addressId") Long addressId) {
        addressService.deleteById(addressId);
        return ResultModel.success();
    }

    @RequestMapping(value = "updateAddress", method = RequestMethod.POST)
    public Object updateAddress(Address address) {
        if (address.getId() == null || address.getUserId() == null) {
            return ResultModel.fail(1011, "缺少参数");
        }
        int count = addressService.countAddress(address.getUserId());

        if (count < 1) {
            address.setIsDefault(1);
        } else {
            address.setIsDefault(0);
        }
        addressService.update(address);
        return ResultModel.success();
    }

    @RequestMapping(value = "getAddress", method = RequestMethod.GET)
    public Object getAddress(@RequestParam("addressId") Long addressId) {
        Address address = addressService.getAddress(addressId);
        return ResultModel.success(address);
    }

    @RequestMapping(value = "listAddress", method = RequestMethod.GET)
    public Object listAddress(@RequestParam("userId") Long userId,
                              @RequestParam("pn") Integer pn,
                              @RequestParam("ps") Integer ps) {

        Page<Address> page = PageHelper.startPage(pn, ps);
        List<Address> adressList = addressService.listAddress(userId);
        long total = page.getTotal();
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("adressList", adressList);
        map.put("total", total);
        return ResultModel.success(map);
    }

    @RequestMapping(value = "{addressId}", method = RequestMethod.GET)
    public Object getAddressById(@PathVariable("addressId") Long addressId) {
        Address address = addressService.getAddressFromCache(addressId);
        log.info("getAddressById address1 <== " + address);
        Address cacheAddr = addressService.getAddressFromCache(addressId);
        log.info("getAddressById cacheAddr <== " + cacheAddr);
        address = addressService.getAddressFromCache(addressId);
        log.info("getAddressById address2 <== " + address);
        return ResultModel.success(address);
    }

    @RequestMapping(value = "{addressId}", method = RequestMethod.DELETE)
    public Object deleteAddressById(@PathVariable("addressId") Long addressId) {
        addressService.deleteFromCache(addressId);
        return "删除成功";
    }
}
