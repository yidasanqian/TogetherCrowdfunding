package me.yidasanqian.crowdfunding.app;

import me.yidasanqian.crowdfunding.domain.Order;
import me.yidasanqian.crowdfunding.domain.Reward;
import me.yidasanqian.crowdfunding.service.IOrderService;
import me.yidasanqian.crowdfunding.service.IRewardService;
import me.yidasanqian.crowdfunding.vo.ResultModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Linyu Chen
 */
@RestController
@RequestMapping("api/v1/reward")
public class RewardApi {

    @Resource
    private IRewardService rewardService;

    @Resource
    private IOrderService orderService;

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Object add(Reward reward) {
        rewardService.save(reward);
        return ResultModel.success();
    }

    /**
     * 支持众筹项目
     * 创建订单信息
     *
     * @param userId    支持者id
     * @param rewardId
     * @param addressId 有偿回报的地址id
     * @param count     有偿回报的数量
     * @param remark    备注
     * @param money     支持的金额
     * @return
     */
    @RequestMapping(value = "supportFunding", method = RequestMethod.POST)
    public Object supportFunding(@RequestParam("userId") Long userId,
                                 @RequestParam("rewardId") Long rewardId,
                                 @RequestParam(value = "addressId", required = false) Long addressId,
                                 @RequestParam(value = "count", required = false) Integer count,
                                 @RequestParam(value = "remark", required = false) String remark,
                                 @RequestParam("money") BigDecimal money) {

        if (count == null) {
            count = 1;
        }

        Reward reward = rewardService.getReward(rewardId);
        Reward newReward = reward;

        Integer originSupportNumber = reward.getSupportNumber();
        if (originSupportNumber == null) {
            originSupportNumber = 0;
        }
        int sn = originSupportNumber;

        int type = reward.getType();
        // 无偿支持的情况
        if (type == 0) {
            sn += 1;
        } else if (type == 1) {  // 有偿支持的情况
            sn += 1;
            Integer originLimitNumber = reward.getLimitNumber();
            if (originLimitNumber == null) {
                originLimitNumber = 1000000;
            }
            int ln = originLimitNumber;
            // 如果支持人数大于限制的人数，则返回错误提示
            if (sn > ln) {
                return ResultModel.fail(1013, "支持人数已到达上限");
            }

            money = money.multiply(BigDecimal.valueOf(count));

            newReward.setAddressId(addressId);
        } else {
            // todo 其他有偿情况
            return ResultModel.fail(2000, "暂不支持该功能");
        }

        Order order = new Order();
        order.setAmount(money);
        order.setRewardId(rewardId);
        order.setUserId(userId);
        order.setFundId(reward.getFundId());
        order.setNumber(count);
        order.setStatus(1);
        //order.setCreatetime(new Date());
        orderService.save(order);

        newReward.setUserId(userId);
        newReward.setSupportNumber(sn);
        newReward.setRemark(remark);
        rewardService.save(newReward);

        return ResultModel.success(order);
    }

    @RequestMapping(value = "listByFundId", method = RequestMethod.GET)
    public Object listByFundId(@RequestParam("fundId") Long fundId) {
        List<Reward> rewardList = rewardService.listByFundId(fundId);
        return ResultModel.success(rewardList);
    }

    @RequestMapping(value = "getById", method = RequestMethod.GET)
    public Object getById(@RequestParam("rewardId") Long rewardId) {
        Reward reward = rewardService.getReward(rewardId);
        return ResultModel.success(reward);
    }
}
