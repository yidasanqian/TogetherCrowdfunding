package me.yidasanqian.crowdfunding.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * <a href=https://github.com/springfox/springfox-demos/blob/master/boot-swagger/src/main/java/springfoxdemo/boot/swagger/Application.java>Application.java</a>
 * @author Linyu Chen
 */
@Configuration
public class Swagger2Config {

    @Bean
    public Docket addressDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enableUrlTemplating(true)
                .ignoredParameterTypes(ApiIgnore.class)
                .groupName("address-api")
                .apiInfo(addressApiInfo())
                .select()
                .paths(addressPaths())
                .build();
    }

    private Predicate<String> addressPaths() {
        /*return new Predicate<String>() {
            @Override
            public boolean apply(String s) {
                return s.contains("/api/v1/address");
            }
        };*/
        return or(
                regex("/api/v1/address.*"),
                regex("/api/v1/user.*")
        );
    }

    private ApiInfo addressApiInfo() {
        return new ApiInfoBuilder()
                .title("Address API")
                .description("地址接口")
                .termsOfServiceUrl("http://springfox.io")
                .contact(new Contact("北海", "beihai url", "beihai@qq.com"))
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")
                .version("1.0")
                .build();
    }
}
