package me.yidasanqian.crowdfunding.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;

/**
 * @author Linyu Chen
 */
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {

    @Resource
    private ApiInterceptor apiInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns用于添加拦截规则
        // excludePathPatterns用户排除拦截
        registry.addInterceptor(apiInterceptor)
                .excludePathPatterns("/api/v1/user_authc/**",
                        "/api/v1/alipay/checkSyncNotify",
                        "/api/v1/alipay/checkAsyncNotify")
                .addPathPatterns("/api/v1/**"); // 对来自/api/v1/** 这个链接来的请求进行拦截
    }
}
