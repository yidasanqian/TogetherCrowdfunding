package me.yidasanqian.crowdfunding;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 部署到JavaEE容器
 *
 * @author Linyu Chen
 */
public class CrowdFundingServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(CrowdFundingApplication.class);
    }
}
