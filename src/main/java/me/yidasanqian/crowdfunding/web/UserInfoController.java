package me.yidasanqian.crowdfunding.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Linyu Chen
 */
@Controller
@RequestMapping("user")
public class UserInfoController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 用户查询.
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    //@RequiresPermissions("userInfo:view")//权限管理;
    public String userInfo() {
        log.info("用户查询");
        return "user_info";
    }

    /**
     * 用户添加;
     *
     * @return
     */
    @RequestMapping(value = "add")
    @RequiresPermissions("userInfo:add")//权限管理;
    public String userInfoAdd() {
        log.info("用户添加");
        return "user_info_add";
    }

    /**
     * 用户删除;
     *
     * @return
     */
    @RequestMapping(value = "del")
    @RequiresPermissions("userInfo:del")//权限管理;
    public String userDel() {
        log.info("用户删除");
        return "user_info_del";
    }
}
