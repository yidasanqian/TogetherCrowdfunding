package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Role;

/**
 * @author Linyu Chen
 */
public interface IRoleService {

    Role createRole(Role role);

    void deleteRole(Long roleId);

    // 添加角色-权限之间关系
    void correlationPermissions(Long roleId, Long... permissionIds);

    // 移除角色-权限之间关系
    void uncorrelationPermissions(Long roleId, Long... permissionIds);

}
