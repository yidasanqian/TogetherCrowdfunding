package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Media;

/**
 * @author Linyu Chen
 */
public interface IMediaService {
    void save(Media media);

    Media selectByPrimaryKey(Long mediaId);

    void update(Media media);
}
