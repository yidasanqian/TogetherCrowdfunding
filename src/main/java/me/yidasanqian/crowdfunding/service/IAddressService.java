package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Address;

import java.util.List;

/**
 * @author Linyu Chen
 */
public interface IAddressService {
    void save(Address address);

    void deleteById(Long addressId);

    void update(Address address);

    Address getAddress(Long addressId);

    int countAddress(Long userId);

    List<Address> listAddress(Long userId);

    Address getAddressFromCache(Long addressId);

    void deleteFromCache(Long addressId);
}
