package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Permission;

/**
 * @author Linyu Chen
 */
public interface IPermissionService {
    Permission createPermission(Permission permission);

    void deletePermission(Long permissionId);
}
