package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.UserAuthc;

/**
 * @author Linyu Chen
 */
public interface IUserAuthcService {
    String getIdentifier(String identifier);

    void save(UserAuthc userAuthc);

    UserAuthc getUserAuthc(String username, String password);

    UserAuthc getUserAuthc(String username);

    void registerUser(UserAuthc userAuthc, String defaultAvatarUrl);
}
