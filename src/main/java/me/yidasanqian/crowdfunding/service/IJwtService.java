package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Jwt;

/**
 * @author Linyu Chen
 */
public interface IJwtService {
    void save(Jwt jwt);

    boolean isExist(String token);

    void deleteByUserId(Long userId);

    Jwt getByUserId(Long userId);

    void update(Jwt jwt);
}
