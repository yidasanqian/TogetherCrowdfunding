package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.CrowdFund;
import me.yidasanqian.crowdfunding.domain.Progress;

import java.util.List;

/**
 * @author Linyu Chen
 */
public interface ICrowdFundingService {
    void save(CrowdFund crowdFund);

    void deleteById(Long fundId);

    void update(CrowdFund crowdFund);

    List<CrowdFund> listCrowdFund();

    CrowdFund getCrowdFund(Long fundId);

    void updateProgress(Progress progress);

    void saveProgress(Progress progress);

    List<CrowdFund> listByUserId(Long userId);

    CrowdFund getByUserIdAndFundId(Long userId, Long fundId);
}
