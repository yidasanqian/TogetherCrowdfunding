package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.CrowdFund;
import me.yidasanqian.crowdfunding.domain.Progress;
import me.yidasanqian.crowdfunding.mapper.CrowdFundMapper;
import me.yidasanqian.crowdfunding.mapper.ProgressMapper;
import me.yidasanqian.crowdfunding.service.ICrowdFundingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class CrowdFundingService implements ICrowdFundingService {

    @Resource
    private CrowdFundMapper crowdFundMapper;

    @Resource
    private ProgressMapper progressMapper;

    @Override
    public void save(CrowdFund crowdFund) {
        crowdFundMapper.insert(crowdFund);
    }

    @Override
    public void deleteById(Long fundId) {
        crowdFundMapper.deleteByPrimaryKey(fundId);
    }

    @Override
    public void update(CrowdFund crowdFund) {
        crowdFundMapper.updateByPrimaryKey(crowdFund);
    }

    @Override
    public List<CrowdFund> listCrowdFund() {
        return crowdFundMapper.selectAll();
    }

    public List<CrowdFund> listByUserId(Long userId) {
        return crowdFundMapper.listByUserId(userId);
    }

    public void saveProgress(Progress progress) {
        progressMapper.insert(progress);
    }

    @Override
    public CrowdFund getCrowdFund(Long fundId) {
        return crowdFundMapper.selectByPrimaryKey(fundId);
    }

    public CrowdFund getByUserIdAndFundId(Long userId, Long fundId) {
        return crowdFundMapper.getByUserIdAndFundId(userId, fundId);
    }

    public void updateProgress(Progress progress) {
        progressMapper.updateByPrimaryKey(progress);
    }
}
