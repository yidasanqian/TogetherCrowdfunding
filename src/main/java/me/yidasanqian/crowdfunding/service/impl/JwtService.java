package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Jwt;
import me.yidasanqian.crowdfunding.mapper.JwtMapper;
import me.yidasanqian.crowdfunding.service.IJwtService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class JwtService implements IJwtService {

    @Resource
    private JwtMapper jwtMapper;

    @Override
    public void save(Jwt jwt) {
        jwtMapper.insert(jwt);
    }

    @Override
    public boolean isExist(String token) {
        return jwtMapper.isExist(token);
    }

    @Override
    @CacheEvict(value = "CONSTANT", key = "'jwt_userId'+#userId")
    public void deleteByUserId(Long userId) {
        jwtMapper.deleteByUserId(userId);
    }

    @Override
    @Cacheable(value = "CONSTANT", key = "'jwt_userId'+#userId")
    public Jwt getByUserId(Long userId) {
        return jwtMapper.getByUserId(userId);
    }

    @Override
    @CachePut(value = "CONSTANT", key = "'jwt_userId'+#jwt.userId")
    public void update(Jwt jwt) {
        jwtMapper.update(jwt);
    }
}
