package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Permission;
import me.yidasanqian.crowdfunding.domain.Role;
import me.yidasanqian.crowdfunding.domain.User;
import me.yidasanqian.crowdfunding.mapper.RoleMapper;
import me.yidasanqian.crowdfunding.mapper.UserMapper;
import me.yidasanqian.crowdfunding.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * @author Linyu Chen
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserService implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Override
    public void save(User user) {
        userMapper.insert(user);
    }

    @Override
    public Long getUserId(Long userAuthcId) {
        return userMapper.getUserId(userAuthcId);
    }

    @Override
    public User selectByPrimaryKey(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public void update(User user) {
        userMapper.updateByPrimaryKey(user);
    }

    @Override
    public User getUserByAuthcId(Long authcId) {
        return userMapper.getUserByAuthcId(authcId);
    }

    @Override
    public List<Role> listRolesByUserId(Long userId) {
        return roleMapper.listRolesByUserId(userId);
    }

    @Override
    public List<Permission> listPermissionsByRoleId(Long roleId) {
        return roleMapper.listPermisionsByRoleId(roleId);
    }

    @Override
    public List<Role> listRolesByUsername(String username) {
        return roleMapper.listRolesByUsername(username);
    }

    @Override
    public void correlationRoles(Long userId, Long... roleIds) {
        // todo 建立用户角色关系
    }

    @Override
    public void uncorrelationRoles(Long userId, Long... roleIds) {
        // todo 解除用户角色关系

    }

    @Override
    public Set<String> findRoles(String username) {
        // todo 根据用户名查找所有角色名

        return null;
    }

    @Override
    public Set<String> findPermissions(String username) {
        // todo 根据用户名查找所有权限名

        return null;
    }
}
