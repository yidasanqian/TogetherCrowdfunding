package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Reward;
import me.yidasanqian.crowdfunding.mapper.RewardMapper;
import me.yidasanqian.crowdfunding.service.IRewardService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class RewardService implements IRewardService {
    @Resource
    private RewardMapper rewardMapper;

    @Override
    public void save(Reward reward) {
        rewardMapper.insert(reward);
    }

    @Override
    @Cacheable(value = "CONSTANT", key = "'Reward_' + #rewardId")
    public Reward getReward(Long rewardId) {
        return rewardMapper.selectByPrimaryKey(rewardId);
    }

    @Cacheable(value = "CONSTANT", key = "'RewardList_fundId' + #fundId")
    public List<Reward> listByFundId(Long fundId) {
        return rewardMapper.listByFundId(fundId);
    }
}
