package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Media;
import me.yidasanqian.crowdfunding.mapper.MediaMapper;
import me.yidasanqian.crowdfunding.service.IMediaService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class MediaService implements IMediaService {

    @Resource
    private MediaMapper mediaMapper;

    @Override
    public void save(Media media) {
        mediaMapper.insert(media);
    }

    @Override
    @Cacheable(value = "CONSTANT", key = "'media_'+#mediaId")
    public Media selectByPrimaryKey(Long mediaId) {
        return mediaMapper.selectByPrimaryKey(mediaId);
    }

    @Override
    @CachePut(value = "CONSTANT", key = "'media_'+#media.id")
    public void update(Media media) {
        mediaMapper.updateByPrimaryKey(media);
    }
}
