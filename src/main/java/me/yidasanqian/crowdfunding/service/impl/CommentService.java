package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Comment;
import me.yidasanqian.crowdfunding.mapper.CommentMapper;
import me.yidasanqian.crowdfunding.service.ICommentService;
import me.yidasanqian.crowdfunding.vo.CommentVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class CommentService implements ICommentService {

    @Resource
    private CommentMapper commentMapper;

    @Override
    public void save(Comment comment) {
        commentMapper.insert(comment);
    }

    public List<CommentVo> listCommentsByFundId(Long fundId) {
        return commentMapper.listCommentsByFundId(fundId);
    }
}
