package me.yidasanqian.crowdfunding.service.impl;

import me.yidasanqian.crowdfunding.domain.Order;
import me.yidasanqian.crowdfunding.domain.PayOrder;
import me.yidasanqian.crowdfunding.mapper.OrderMapper;
import me.yidasanqian.crowdfunding.mapper.PayOrderMapper;
import me.yidasanqian.crowdfunding.service.IOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Linyu Chen
 */
@Service
@Transactional
public class OrderService implements IOrderService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private PayOrderMapper payOrderMapper;

    @Override
    public void save(Order order) {
        orderMapper.insert(order);
    }

    public List<Order> listOrder(Long userId, Integer status) {
        return orderMapper.listOrder(userId, status);
    }

    @Override
    public Order getByOutTradeNo(String outTradeNo) {
        return orderMapper.getByOutTradeNo(outTradeNo);
    }

    @Override
    public void savePay(PayOrder payOrder) {
        payOrderMapper.insert(payOrder);
    }

    @Override
    public void updateOrder(Order order) {
        orderMapper.updateByPrimaryKey(order);
    }
}
