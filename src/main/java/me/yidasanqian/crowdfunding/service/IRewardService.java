package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Reward;

import java.util.List;

/**
 * @author Linyu Chen
 */
public interface IRewardService {
    void save(Reward reward);

    Reward getReward(Long rewardId);

    List<Reward> listByFundId(Long fundId);
}
