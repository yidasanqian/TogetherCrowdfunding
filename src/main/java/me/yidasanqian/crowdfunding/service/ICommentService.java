package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Comment;
import me.yidasanqian.crowdfunding.vo.CommentVo;

import java.util.List;

/**
 * @author Linyu Chen
 */
public interface ICommentService {
    void save(Comment comment);

    List<CommentVo> listCommentsByFundId(Long fundId);
}
