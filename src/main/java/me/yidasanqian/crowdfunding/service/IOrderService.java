package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Order;
import me.yidasanqian.crowdfunding.domain.PayOrder;

import java.util.List;

/**
 * @author Linyu Chen
 */
public interface IOrderService {
    void save(Order order);

    Order getByOutTradeNo(String outTradeNo);

    void savePay(PayOrder payOrder);

    void updateOrder(Order order);

    List<Order> listOrder(Long userId, Integer status);
}
