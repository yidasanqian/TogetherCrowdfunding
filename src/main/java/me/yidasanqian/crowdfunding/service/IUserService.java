package me.yidasanqian.crowdfunding.service;

import me.yidasanqian.crowdfunding.domain.Permission;
import me.yidasanqian.crowdfunding.domain.Role;
import me.yidasanqian.crowdfunding.domain.User;

import java.util.List;
import java.util.Set;

/**
 * @author Linyu Chen
 */
public interface IUserService {
    void save(User user);

    Long getUserId(Long userAuthcId);

    User selectByPrimaryKey(Long userId);

    void update(User user);

    User getUserByAuthcId(Long authcId);

    List<Role> listRolesByUserId(Long userId);

    List<Permission> listPermissionsByRoleId(Long roleId);

    List<Role> listRolesByUsername(String username);

    void correlationRoles(Long userId, Long... roleIds); //添加用户-角色关系

    void uncorrelationRoles(Long userId, Long... roleIds);// 移除用户-角色关系

    Set<String> findRoles(String username);// 根据用户名查找其角色

    Set<String> findPermissions(String username); //根据用户名查找其权限
}
